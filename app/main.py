import json
from fastapi import FastAPI
from mangum import Mangum
import boto3

app = FastAPI()
database = boto3.resource("dynamodb")
table = database.Table("flask")


@app.get("/hello")
def hello():
    return {"Hello": "World"}


@app.get("/users")
def list_users():
    items = table.scan()["Items"]
    return {"items": json.dumps(items)}


handler = Mangum(app, enable_lifespan=False)
